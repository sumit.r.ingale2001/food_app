import Products from '../models/productModel.js'


// function to get all the products from the database start
export const getProducts = async (req, res) => {
    try {
        const product = await Products.find();
        res.status(200).json(product)
    } catch (error) {
        res.status(error, 'error while calling getProducts api')
    }
}
// function to get all the products from the database end

// function to post products from the database start
export const postProduct = async (req, res) => {
    const data = req.body;
    try {
        const product = new Products(data);
        await product.save();
        res.status(200).json(product)
    } catch (error) {
        res.status(500).json(error, "Error while posting data")
    }
}
// function to post products from the database end


// function to get the product by id start 
export const getProductById = async (req, res) => {
    try {
        const id = req.params.id;
        const product = await Products.findById(id);
        res.status(200).json(product)
    } catch (error) {
        res.status(500).json(error, 'error while calling getProductById api');
    }
}
// function to get the product by id end