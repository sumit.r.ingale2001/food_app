import mongoose from 'mongoose'

// creating model for the products or the food items that must have title, description, image, price
const ProductSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        max: 50,
    },
    desc: {
        type: String,
        required: true,
        max: 200
    },
    img: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true
    },
    prices: {
        type: Array,
        required: true,
    },
    extra: {
        type: [
            {
                text: { type: String, required: true },
                price: { type: Number, required: true }
            },
        ]
    }
},
    { timestamps: true }
)

// exporting the product schema after making a model
const Products = mongoose.model("product", ProductSchema);
export default Products;