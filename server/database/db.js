import mongoose from 'mongoose';
import dotenv from 'dotenv'

// Load environment variables
dotenv.config();
const MONGO_URL = process.env.MONGO_URL;

// setting up the database connection 
const Connection = async () => {
    // connecting to mongodb using the mongoose library 
    try {
        await mongoose.connect(MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        console.log("Database connected successfully")
    } catch (error) {
        // if there is error this catch block will catch it and will console the error 
        console.log(error, "Error while connecting to the database")
    }
}

export default Connection;