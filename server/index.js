import express from 'express';
import cors from 'cors';
import Connection from './database/db.js';
import productRoutes from './routes/productRoutes.js'

// port for express server 
const PORT = process.env.PORT || 8000;
// initializing express app 
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// cors for cross origin resourse sharing issue with front end and backend  
app.use(cors());

// routes 
app.use('/product',productRoutes);

// calling the databse function here 
Connection();

// starting the server 
app.listen(PORT, () => {
    console.log(`server running successfully on port ${PORT}`)
})
