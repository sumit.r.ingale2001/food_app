import { Router } from 'express';
import { getProductById, getProducts, postProduct } from '../controllers/productController.js';

const router = Router();

// route to get all products 
router.get('/', getProducts)

// route to post product 
router.post('/', postProduct)

// route to get the product by id 
router.get("/:id", getProductById)


export default router;