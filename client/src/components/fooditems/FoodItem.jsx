/* eslint-disable react/prop-types */

import CircleIcon from '@mui/icons-material/Circle';
import { useNavigate } from 'react-router-dom';
import { CardContainer, CardImage, CardContent } from './styles';
import { Typography } from '@mui/material';

// single food item component 

const FoodItem = ({ item }) => {
    const navigate = useNavigate()
    return (
        // card start 
        <CardContainer onClick={() => navigate(`/product/${item._id}`)} >
            {/* food item image start  */}
            <CardImage>
                <img src={item.img} alt="" />
            </CardImage>
            {/* food item image end */}

            {/* food content start  */}
            <CardContent>

                {/* food name start  */}
                <Typography variant='h5' >{item.title}</Typography>
                {/* food name end */}

                {/* food description start  */}
                <Typography variant='body2' >{item.desc}</Typography>
                {/* food description end */}

                {/* food price start  */}
                <Typography>RS.{item.prices[0]}</Typography>
                {/* food price end */}

                {/* food category start  */}
                <Typography
                    sx=
                    {{
                        color: item.category === "Nonveg" ? "red" : "green", display: "flex",
                        alignItems: "center"
                    }}
                >
                    <CircleIcon />
                    <span>{item.category}</span>
                </Typography>
                {/* food category end */}
            </CardContent>
            {/* food content end */}
        </CardContainer>
        // card end
    )
}
export default FoodItem;
