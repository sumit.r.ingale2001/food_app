/* eslint-disable react-refresh/only-export-components */
/* eslint-disable react/prop-types */
import { useDispatch, useSelector } from "react-redux"
import FoodItem from "./FoodItem"
import { Container } from "./styles"
import { useEffect } from "react"
import { getAllProducts } from "../../services"


const FoodItems = () => {
    // fetching the data from the redux store 
    const allProducts = useSelector((state) => state.product.items);
    // console.log(allProducts)

    const dispatch = useDispatch()

    // componentDidMount react lifecycle
    useEffect(() => {
        dispatch(getAllProducts())
    }, [])

    return (
        <Container>
            {
                // mapping the data and sending it as a prop 
                allProducts?.map((item, index) => (
                    <FoodItem key={index} item={item} />
                ))
            }
        </Container>
    )
}

export default FoodItems
