/* eslint-disable no-unused-vars */
// styles for the food items 
import { Box, styled } from "@mui/material";

export const Container = styled(Box)(({ theme }) => ({
    maxWidth: "90vw",
    width: "80%",
    margin: "50px auto",
    display: "flex",
    flexWrap: "wrap",
    alignItems: 'center',
    justifyContent: 'start',
    gap: "10px",
}))

export const CardContainer = styled(Box)(({ theme }) => ({
    width:"300px",
    height:"25rem",
    background:"rgba(225,225,225)",
    borderRadius:"5px",
    overflow:"hidden",
    cursor:"pointer",
    display:"flex",
    flexDirection:"column",
}))

export const CardImage = styled(Box)(({ theme }) => ({
width:"95%",
height:"60%",
margin:"5px auto",
"& > img":{
    objectFit:"cover",
    height:"100%",
    width:"100%"
}
}))
export const CardContent = styled(Box)(({ theme }) => ({
display:"flex",
flexDirection:"column",
justifyContent:"space-between",
height:"40%",
padding:"0.8rem 1rem",
"p > span":{
    marginLeft:"0.5rem"
},
"h5":{
    fontWeight:"bold"
}
}))