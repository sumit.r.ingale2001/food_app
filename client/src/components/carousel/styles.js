// styles for the carousel 

import { Box, styled } from '@mui/material'

export const Container = styled(Box)`
height:100vh;
width:100vw;
position:absolute;
top:0;
left:0;
z-index:-10;
filter:brightness(50%);
& > div{
    & img{
        height:100vh;
        width:100vw;
        object-fit:cover;
        mask-image:linear-gradient(180deg, #242428, transparent);
        -webkit-mask-image:linear-gradient(180deg, #242428 transparent);
    }
}
`
