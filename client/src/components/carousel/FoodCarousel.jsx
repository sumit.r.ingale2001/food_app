import { Container } from "./styles"
import Carousel from 'react-multi-carousel'
import { carouselImages } from "../../data";
import "react-multi-carousel/lib/styles.css";


// responsive breakpoints of the react multi carousel 
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

const FoodCarousel = () => {
    return (
        <Container>
            {/* carousel component from react-multi-carousel */}
            <Carousel
                swipeable={false}
                draggable={false}
                responsive={responsive}
                infinite={true}
                arrows={false}
                autoPlay
                autoPlaySpeed={3000}
                customTransition="all .5"
                transitionDuration={500}
                itemClass="carousel-item-padding-40-px" >
                {
                    carouselImages.map((image, index) => (
                        <img src={image.img} key={index} alt="food images" />
                    ))
                }
            </Carousel>
        </Container>
    )
}

export default FoodCarousel
