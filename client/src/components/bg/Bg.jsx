// background for the login signup page 
const Bg = () => {
    return (
        <img
            style={{
                height: "100%",
                width: "100%",
                objectFit: "cover",
                position: "absolute",
                top: 0, left: 0,
                filter: "brightness(35%)",
                zIndex: -3,
            }}
            src="https://plus.unsplash.com/premium_photo-1663852297801-d277b7af6594?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt="" />
    )
}

export default Bg
