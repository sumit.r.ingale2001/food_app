// styles for the form component 
import { Paper, styled } from '@mui/material'

export const Container = styled(Paper)`
padding:15px 35px;
max-width:500px;
width:80%;
background:rgba(225,225,225,0.6);
backdrop-filter:blur(5px);
& > h4{
    text-align:center;
    font-weight:bold;
    margin-top:10px;
    & > a{
        text-decoration:none;
    }
}
& > p{
    text-align:center;
    font-weight:bold;
    color:gray;
    letter-spacing:3px;
}
& > h6{
    font-size:14px;
    text-align:center;
}
& h6 > a{
    text-decoration:none;
    color:#EF5350;
}
& > button{
    display:flex;
    align-items:center;
    border-radius: 20px;
    backdrop-filter:blur(20px);
}
& button > span{
    margin-left:20px;
}
input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
`
export const FormContainer = styled("form")`
display:flex;
flex-direction:column;
margin-top:15px;
& > div{
    margin:5px 0;
    display:flex;
    align-items:center; 
};
& div>span{
    font-size:14px;
    margin-left:10px;
}
& > button{
    background:#EF5350;
    color:white;
    &:hover{
        background:#EF5360;
    }
}
`