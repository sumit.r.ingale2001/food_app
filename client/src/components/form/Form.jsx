/* eslint-disable react/prop-types */
import { Box, Button, Checkbox, TextField, Typography } from "@mui/material"
import GoogleIcon from '@mui/icons-material/Google';
import { Link, useNavigate } from "react-router-dom"
import { Container, FormContainer } from "./styles"
import { useState } from "react"
import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signInWithPopup } from 'firebase/auth'
import { firebaseAuth, googleProvider } from '../../utils/firebase-config'


// a single form component , which can be reused for login signup and for the restaurant

const Form = ({ heading, fullname, username, email, password, btn, btnText, text, linkText, link, phoneNumber }) => {

    // a state for showing and hiding the password 
    const [showPassword, setShowPassword] = useState(false);
    // useNavigate to navigate to different page 
    const navigate = useNavigate()


    // an object where all the values will be stored and will be used for the signin and login purpose
    const [formValues, setFormValues] = useState({
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        password: "",
        number: ""
    })

    // a function that first checks whether the user has to  login or signup 
    const handleSubmit = () => {
        if (heading === 'Sign up') {
            // if the user has to signup the code below will execute 
            const signup = async () => {
                try {
                    const { email, password } = formValues;
                    await createUserWithEmailAndPassword(firebaseAuth, email, password)
                } catch (error) {
                    console.log(error, 'error while signing up user')
                }
            }
            // calling the function 
            signup();
            // after the successfull signin the  user will be navigated to the homepage 
            onAuthStateChanged(firebaseAuth, (currentUser) => {
                if (currentUser) navigate('/')
            })
        }
        else if (heading === 'Login') {
            // if the user has to login the code below will execute 
            const login = async () => {

                try {
                    const { email, password } = formValues;
                    await signInWithEmailAndPassword(firebaseAuth, email, password)
                } catch (error) {
                    console.log(error, 'error while login ')
                }
            }
            // calling the function 
            login();
            // after the successfull login the  user will be navigated to the homepage 
            onAuthStateChanged(firebaseAuth, (currentUser) => {
                if (currentUser) navigate("/")
            })
        }
    }


    // function to login with google 
    const handleClick = async () => {
        try {
            await signInWithPopup(firebaseAuth, googleProvider);
            onAuthStateChanged(firebaseAuth, (currentUser) => {
                if (currentUser) navigate('/')
            })
        } catch (error) {
            console.log(error, 'error while logging in with google')
        }
    }

    return (
        <>
            {/* container start */}
            <Container>
                {/* heading of the form start */}
                <Typography variant="h4" >
                    {heading}
                </Typography>
                {/* heading of the form end  */}

                {/* form container start  */}
                <FormContainer>
                    {
                        // as we have passed the props for the component if the props exist or is true then the element will render, it is applicable for all the input fields 
                        fullname && (
                            <Box>
                                <TextField
                                    value={formValues.firstName}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                    type="text"
                                    fullWidth
                                    name="firstName"
                                    label="Enter your first name"
                                />
                                <TextField
                                    value={formValues.lastName}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                    fullWidth
                                    type="text"
                                    name="lastName"
                                    label="Enter your Last name"
                                    sx={{ ml: "5px" }}
                                />
                            </Box>
                        )
                    }
                    {
                        phoneNumber && (
                            <Box>
                                <TextField
                                    value={formValues.number}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                    type="number"
                                    fullWidth
                                    name="number"
                                    label="Enter your number"
                                />
                            </Box>
                        )
                    }
                    {
                        username && (
                            <Box>
                                <TextField
                                    value={formValues.username}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                    type="text"
                                    name="username"
                                    fullWidth
                                    label="Enter your username"
                                />
                            </Box>
                        )
                    }
                    {
                        email && (
                            <Box>
                                <TextField
                                    value={formValues.email}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                    type="email"
                                    name="email"
                                    fullWidth
                                    label="Enter your email"
                                />
                            </Box>
                        )
                    }
                    {
                        password && (
                            <>
                                <Box>
                                    <TextField
                                        value={formValues.password}
                                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                        type={showPassword ? "text" : "password"}
                                        name="password"
                                        fullWidth
                                        label="Enter your password"
                                    />
                                </Box>
                                <Box>
                                    <Checkbox
                                        size="medium"
                                        sx={{ p: 0 }}
                                        onChange={() => { console.log("checked") }}
                                        onClick={() => setShowPassword(!showPassword)}
                                        type="checkbox"
                                        checked={showPassword ? true : false} />
                                    <span>Show password</span>
                                </Box>
                            </>
                        )
                    }
                    {
                        btn && (
                            <Button
                                size="small"
                                fullWidth
                                onClick={handleSubmit}
                            >{btnText}</Button>
                        )
                    }
                </FormContainer>
                {/* form contianer end  */}

                {/* this typography contains the text for the link and well as the link to the component for eg:- already a member? login */}
                <Typography variant="h6">
                    {text}&nbsp;<Link to={`/${link}`} >{linkText}</Link>
                </Typography>

                {/* button for google login/signin start  */}
                <Button
                    size="medium"
                    color="inherit"
                    fullWidth
                    onClick={handleClick}
                >
                    <GoogleIcon color='error' fontSize="small" />
                    <span>
                        {btnText}&nbsp;with google
                    </span>
                </Button>
                {/* button for google login/signup end  */}
            </Container>
            {/* container end */}
        </>
    )
}

export default Form;
