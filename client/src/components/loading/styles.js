/* eslint-disable no-unused-vars */
import { Box, styled } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    height: "100vh",
    width: "100vw",
    background:"rgba(0,0,0,0.5)",
    backdropFilter:"blur(10px)",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    "& > img":{
        height:"250px",
        width:"250px",
        mixBlendMode:"darken"
    }
}))