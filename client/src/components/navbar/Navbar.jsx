/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-key */
import { Container, StyledDrawer } from './styles';
import { Box, IconButton, AppBar } from '@mui/material';
import NavbarLinks from './NavbarLinks';
import { useState } from 'react';
import MenuIcon from '@mui/icons-material/Menu';
import { Link } from 'react-router-dom';
import logo from '../../assets/logo.png'

const Navbar = ({ scrolled }) => {

    // state for the drawer component to toggle it 
    const [open, setOpen] = useState(false);

    // close function to close the drawer 
    const close = () => {
        setOpen(false);
    }
    return (
        <>
            {/* navbar start  */}
            <AppBar
                color='inherit'
                sx={{ boxShadow: "none", background: "none" }}
            >
                {/* toolbar named as container to handle its style  */}
                <Container
                    sx={{ background: scrolled ? "rgba(0,0,0,0.9)" : "none", backdropFilter: scrolled ? "blur(10px)" : "none" }}
                >
                    {/* logo start  */}
                    <Box>
                        <Link to='/' >
                            <img
                                width="150px"
                                src={logo} alt="dine logo" />
                        </Link>
                    </Box>
                    {/* logo end  */}

                    {/* navbar links start  */}
                    <Box
                        sx={{ display: { lg: "block", md: "block", sm: "none", xs: "none" } }}
                    >
                        <NavbarLinks />
                    </Box>
                    {/* navbar links end  */}

                    {/* hamburger icon start as the screen size it below medium the icon will render */}
                    <Box
                        sx={{ display: { md: "none", sm: "block" } }}
                        onClick={() => setOpen(!open)}
                    >
                        <IconButton>
                            <MenuIcon fontSize='large' sx={{color:"#ef5350"}} />
                        </IconButton>
                    </Box>
                </Container>
                {/* drawer start  */}
                <StyledDrawer
                    open={open}
                    onClose={close}
                >
                    <NavbarLinks />
                </StyledDrawer>
                {/* drawer end  */}
            </AppBar>
            {/* navbar end  */}
        </>
    )
}

export default Navbar;

