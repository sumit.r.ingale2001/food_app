/* eslint-disable react/prop-types */
import { Avatar } from '@mui/material'
import { Link } from 'react-router-dom'
import { NavLink } from './styles'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';


const links = [
    { id: 1, to: "/restaurantForm", text: "Add restaurant" },
    { id: 2, to: "/login", text: "Login" },
    { id: 3, to: "/cart", text: `Cart`, icon: <ShoppingCartIcon fontSize='small' /> },
]



const NavbarLinks = () => {
    return (
        <NavLink>
            {
                links.map((link) => (
                    <Link key={link.id} to={link.to} >{link.text}&nbsp;{link.icon ? link.icon : null}</Link>
                ))
            }
            <Link to='/profile' ><Avatar></Avatar></Link>
        </NavLink>
    )
}

export default NavbarLinks
