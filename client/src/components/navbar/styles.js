/* eslint-disable no-unused-vars */
import { AppBar, Box, Drawer, Toolbar, styled } from '@mui/material';

export const StyledDrawer = styled(Drawer)(({ theme }) => ({
    ".css-4t3x6l-MuiPaper-root-MuiDrawer-paper ": {
        background: "black"
    }

}))
export const Container = styled(Toolbar)(({ theme }) => ({
    display: "flex",
    padding: "1rem",
    alignItems: "center",
    position: "relative",
    zIndex: 555,
    justifyContent: "space-between",
}))

export const NavLink = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    '& > a': {
        margin: "0 1rem",
        textDecoration: "none",
        fontSize: "1.1rem",
        display: "flex",
        alignItems: "center",
        color: "#fff",
        ":hover": {
            color: "#ef5350"
        }
    },
    [theme.breakpoints.down("md")]: {
        flexDirection: "column-reverse",
        alignItems: "start",
        padding: "10px 30px",
        "& > a": {
            margin: "10px 0",
            display: "flex",
            alignItems: "center",
        }
    },
    "&  ul": {
        padding: "8px 15px",
    },
}))
