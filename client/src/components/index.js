// common export file for the components folder 

export { default as Bg } from './bg/Bg';
export { default as Form } from './form/Form';
export { default as Navbar } from './navbar/Navbar'
export { default as FoodCarousel } from './carousel/FoodCarousel'
export {default as FoodItems} from './fooditems/FoodItems'
export {default as Loading} from './loading/Loading'
