import { Button } from "@mui/material"
import { ProductDetail, Total } from "./styles"

const CartTotal = () => {
    return (
        // total component start 
        <Total>
            <h1>Cart Total</h1>
            <ProductDetail><span>Subtotal:</span>&nbsp;Rs.400</ProductDetail>
            <ProductDetail><span>Discount:</span>&nbsp;Rs.50</ProductDetail>
            <ProductDetail><span>Total:</span>&nbsp;Rs.350</ProductDetail>
            <Button fullWidth >Checkout now</Button>
        </Total>
        // total component end 
    )
}

export default CartTotal
