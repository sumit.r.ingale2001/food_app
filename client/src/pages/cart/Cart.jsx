import { Grid } from "@mui/material"
import { Navbar } from "../../components/index"
import { useGlobalContext } from "../../context/Context"
import { Container } from "./styles"
import CartTable from "./CartTable"
import CartTotal from "./CartTotal"

const Cart = () => {
    // custom hook for the navbar 
    const { scrolled } = useGlobalContext();
    return (
        <>
            {/* navbar start  */}
            <Navbar scrolled={scrolled} />
            {/* navbar end */}

            {/* gird container start  */}
            <Container container >
                <Grid item lg={8} md={8} sm={12} xs={12} >
                    {/* cart table component start  */}
                    <CartTable />
                    {/* cart table component end */}
                </Grid>
                <Grid item lg={4} sm={12} xs={12} md={4} >
                    {/* cart total component start  */}
                    <CartTotal />
                    {/* cart total component end */}
                </Grid>
            </Container>
            {/* gird container end */}
        </>
    )
}

export default Cart
