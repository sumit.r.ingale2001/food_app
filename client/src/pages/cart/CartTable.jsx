import { Header, Image, ProductDetail } from './styles'
import { Table, TableBody, TableCell, TableRow } from '@mui/material'
const CartTable = () => {
    return (

        // table for the left section of the cart page start 
        <Table>
            {/* table heading start  */}
            <Header>
                {/* table row or the tr element start  */}
                <TableRow>
                    {/* table cells or the td element start */}
                    <TableCell>Product</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Extras</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>Total</TableCell>
                    {/* table cells or the td element end */}
                </TableRow>
                {/* table row or the tr element start end*/}
            </Header>
            {/* table heading end  */}

            {/* table body start  */}
            <TableBody>
                {/* table row or the tr element start  */}
                <TableRow>
                    {/* table row or the td element start  */}
                    <TableCell>
                        <Image>
                            <img src="https://images.unsplash.com/photo-1604382354936-07c5d9983bd3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt="" />
                        </Image>
                    </TableCell>
                    <TableCell>
                        <ProductDetail variant="body2" gutterBottom >Pizza</ProductDetail>
                    </TableCell>
                    <TableCell>
                        <ProductDetail variant="body2" gutterBottom >Coke,Fries</ProductDetail>
                    </TableCell>
                    <TableCell>
                        <ProductDetail variant="body2" gutterBottom >Rs.350</ProductDetail>
                    </TableCell>
                    <TableCell>
                        <ProductDetail variant="body2" gutterBottom >2</ProductDetail>
                    </TableCell>
                    <TableCell>
                        <ProductDetail variant="body2" gutterBottom >Rs.&nbsp;400</ProductDetail>
                    </TableCell>
                    {/* table row or the td element start  */}
                </TableRow>
                {/* table row or the tr element start  */}
            </TableBody>
            {/* table body end  */}
        </Table>
        // table for the left section of the cart page end 
    )
}

export default CartTable
