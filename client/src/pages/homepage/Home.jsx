import { FoodCarousel, Navbar, FoodItems, Loading } from '../../components/index'
import { Container, InputContainer } from './styles';
import { useTypewriter } from 'react-simple-typewriter'
import { useGlobalContext } from '../../context/Context';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProducts } from '../../services';



const Home = () => {

    // using useDispatch to dispatch the getAllProducts function 
    const dispatch = useDispatch()
    // getting the items array from the redux store 
    const data = useSelector((state => state.product.items))

    // created a custom hook for the navbar 
    const { scrolled } = useGlobalContext();
    // type writting effect for the home page 
    const [text] = useTypewriter({
        words: ['Hungry?', 'Game night?', 'Netflix & Chill?', 'Anime binge?'],
        // loop: {},
    })

    useEffect(() => {
        if (data.length === 0) {
            dispatch(getAllProducts())
        }
    }, [])
    return (
        <>
            {
                data.length <= 0 ? (
                    <Loading />
                )
                    : (
                        <>
                            <Container>
                                {/* background carousel component start  */}
                                <FoodCarousel />
                                {/* background carousel component end */}

                                {/* navbar component start  */}
                                <Navbar scrolled={scrolled} />
                                {/* navbar component end */}

                                {/* type writer start  */}
                                <h1>&nbsp;{text}</h1>
                                {/* type writer end */}

                                {/* titles start  */}
                                <h3>Order from your nearest restaurant</h3>
                                <h5>Anything, anytime, anywhere. We deliver it all.</h5>
                                {/* titles end  */}

                                {/* search start  */}
                                <InputContainer>
                                    <input type='text' placeholder='Search Food item' />
                                    <button>Search</button>
                                </InputContainer>
                                {/* search end */}

                            </Container>
                            {/* food items component start  */}
                            <FoodItems />
                            {/* food items component end */}
                        </>
                    )
            }


        </>
    )
}

export default Home;

