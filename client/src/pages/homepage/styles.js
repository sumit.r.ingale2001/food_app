/* eslint-disable no-unused-vars */
import { Box, Grid, styled } from "@mui/material";

export const Container = styled(Box)(({ theme }) => ({
    height: "100vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    padding: "10px",
    "& >img": {
        width: "40rem"
    },
    "h1": {
        fontSize: "8rem",
        color: "#EF5350",
        textAlign: "center",
        marginBottom: "2rem",
        textShadow: "1px 1px 2px #fff"
    },
    "h3": {
        fontSize: "2.5rem",
        color: "white",
        letterSpacing: "3px",
        textAlign: "center",
    },
    "h5": {
        fontSize: "1.2rem",
        color: "white",
        letterSpacing: "3px",
        textAlign: "center",
    },
    [theme.breakpoints.down("md")]: {
        "h1": {
            fontSize: "4.5rem",
        },
        "h3": {
            fontSize: "2rem"
        }
    },
    [theme.breakpoints.down("sm")]: {
        "h1": {
            fontSize: "2rem",
        },
        "h3": {
            fontSize: "1rem"
        },
        "h5": {
            fontSize: "1rem"
        }
    }
}))

export const InputContainer = styled(Box)(({ theme }) => ({
    width: "80%",
    maxWidth: "1000px",
    marginTop: "4rem",
    display: "grid",
    gridTemplateColumns: "2fr 1fr",
    overflow: "hidden",
    borderRadius: "3px",
    "& > *": {
        padding: "1.5rem",
        fontSize: "1.2rem"
    },
    "& > input": {
        background: "rgba(0,0,0,0.7)",
        color: "white",
        outline: "none",
        border: "none"
    },
    "& > button": {
        color: "white",
        background: "#ef5350",
        outline: "none",
        border: "none",
        cursor: "pointer"
    },
    [theme.breakpoints.down("sm")]: {
        gridTemplateColumns: "1fr",
        gap: 10,
        "& > *": {
            padding: "0.5rem",
            fontSize: "0.8rem"
        }
    }
}))