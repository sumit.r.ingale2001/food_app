/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { SignupContainer } from './styles'
import Bg from "../../components/bg/Bg"
import { Form } from '../../components/index'

const Signup = () => {

    return (
        // signup container start 
        <SignupContainer>
            {/* background start  */}
            <Bg />
            {/* background end  */}

            {/* signup form start */}
            <Form
                heading='Sign up'
                fullname
                username
                email
                password
                btn
                btnText='Sign up'
                linkText='Login'
                link='login'
                text='Already a member?'
            />
            {/* signup form end  */}
        </SignupContainer>
        // signup container end 
    )
}

export default Signup
