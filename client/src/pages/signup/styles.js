import { Box, styled } from '@mui/material';

// styles for the signup page
export const SignupContainer = styled(Box)`
display:flex;
position:relative;
justify-content:center;
align-items:center;
height:100vh;
width:100vw;
overflow:hidden;
&  button{
    margin:10px 0;
};
` 
