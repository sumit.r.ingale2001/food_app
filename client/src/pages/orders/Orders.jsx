import { Button, Grid, Table, TableBody, TableCell, TableRow } from '@mui/material';
import { Navbar } from '../../components/index';
import { useGlobalContext } from '../../context/Context'
import { Container, Header, ProductDetail, StatusContainer, Total } from './styles';
import paid from '../../assets/paid.png'
import checked from '../../assets/checked.png'
import bake from '../../assets/bake.png'
import bike from '../../assets/bike.png'
import delivered from '../../assets/delivered.png'
import styles from './orders.module.css'

const Orders = () => {

    const initialStatus = 0
    const currentStatus = (index) => {
        if (index - initialStatus < 1) return styles.done;
        if (index - initialStatus === 1) return styles.preparing
        if (index - initialStatus > 1) return styles.notDone
    }
    const { scrolled } = useGlobalContext();
    return (
        <>
            <Navbar scrolled={scrolled} />
            <Container container >
                <Grid item xs={12} sm={12} md={8} lg={8}  >
                    <Table>
                        <Header>
                            {/* table row or the tr element start  */}
                            <TableRow>
                                {/* table cells or the td element start */}
                                <TableCell>Order ID</TableCell>
                                <TableCell>Customers</TableCell>
                                <TableCell>Address</TableCell>
                                <TableCell>Total</TableCell>
                                {/* table cells or the td element end */}
                            </TableRow>
                            {/* table row or the tr element start end*/}
                        </Header>
                        <TableBody >
                            <TableRow>
                                <TableCell>
                                    12521
                                </TableCell>
                                <TableCell>
                                    Name
                                </TableCell>
                                <TableCell>
                                    Address
                                </TableCell>
                                <TableCell>
                                    Rs 500
                                </TableCell>
                            </TableRow>
                            <TableRow sx={{ background: "white" }} >
                                <TableCell>
                                    <StatusContainer className={currentStatus(0)}>
                                        <img src={paid} alt="payment" height={30} width={30} />
                                        <span>Payment</span>
                                        <img className={styles.icon} src={checked} alt="checked" height={15} width={15} />
                                    </StatusContainer>
                                </TableCell>
                                <TableCell>
                                    <StatusContainer className={currentStatus(1)}>
                                        <img src={bake} alt="Preparing" height={30} width={30} />
                                        <span>Preparing</span>
                                        <img className={styles.icon} src={checked} alt="checked" height={15} width={15} />
                                    </StatusContainer>
                                </TableCell>
                                <TableCell>
                                    <StatusContainer className={currentStatus(2)}>
                                        <img src={bike} alt="delivery" height={30} width={30} />
                                        <span>On the way</span>
                                        <img className={styles.icon} src={checked} alt="checked" height={15} width={15} />
                                    </StatusContainer>
                                </TableCell>
                                <TableCell>
                                    <StatusContainer className={currentStatus(3)}>
                                        <img src={delivered} alt="delivered" height={30} width={30} />
                                        <span>Delivered</span>
                                        <img className={styles.icon} src={checked} alt="checked" height={15} width={15} />
                                    </StatusContainer>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item xs={12} sm={12} md={4} lg={4}  >
                    <Total>
                        <h1>Cart Total</h1>
                        <ProductDetail><span>Subtotal:</span>&nbsp;Rs.400</ProductDetail>
                        <ProductDetail><span>Discount:</span>&nbsp;Rs.50</ProductDetail>
                        <ProductDetail><span>Total:</span>&nbsp;Rs.350</ProductDetail>
                        <Button fullWidth disabled >Paid</Button>
                    </Total>
                </Grid>
            </Container>
        </>
    )
}

export default Orders
