/* eslint-disable no-unused-vars */

// orders page styles 
import { Box, Card, Grid, TableHead, Typography, styled } from '@mui/material'


export const Container = styled(Grid)(({ theme }) => ({
    padding:"0 3rem",
    "& td": {
        color: "white",
        textAlign: "center"
    },
    "& >div": {
        padding: "8rem 1rem 2rem 1rem",
    }
}))

export const ProductDetail = styled(Typography)(({ theme }) => ({
    fontSize: "1.2rem",
    textAlign: "center",
}))

export const Total = styled(Card)(({ theme }) => ({
    background: "rgba(225,225,225,0.9)",
    backdropFilter: "blur(10px)",
    borderRadius: "5px",
    padding: "20px 35px",
    maxHeight: "400px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "start",
    width: "90%",
    flexDirection: "column",
    "h1": {
        textTransform: "uppercase",
        textAlign: "center",
        marginBottom: "1rem",
    },
    margin: "0 auto",
    "& > p": {
        margin: "10px 0",
        fontSize: "1.3rem",
        "&>span": {
            fontWeight: "bold",
        }
    },
    "& > button": {
        background: "#ef5053",
        color: "white !important",
        margin: "10px 0",
        fontWeight: "bold",
        ":hover": {
            background: "white",
            color: "#ef5053 !important",
        }
    }
}))


export const Header = styled(TableHead)(({ theme }) => ({
    background: "#000",
    "& th": {
        fontSize: "1.3rem",
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
}))

export const StatusContainer = styled(Box)(({ theme }) => ({
color:"black",
display:"flex",
flexDirection:"column",
alignItems:"center",
}))