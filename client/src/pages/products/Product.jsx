/* eslint-disable no-unused-vars */
import { Box, Button, Typography } from "@mui/material"
import { AddToCart, BgContainer, Container, ExtraContainer, ImageContainer, Left, ProductContainer, Right, TitleContainer } from "./styles"
import { Loading, Navbar } from '../../components/index'
import { useGlobalContext } from "../../context/Context"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import CircleIcon from '@mui/icons-material/Circle';
import { useDispatch, useSelector } from "react-redux"
import { getDataById } from "../../services"



const Product = () => {
    // using useDispatch to dispatch the getDataById function 
    const dispatch = useDispatch();

    // getting the id from the url by useParams()
    const { id } = useParams();

    // componentDidMount react lifecycle method 
    useEffect(() => {
        dispatch(getDataById(id));
    }, [])
    
    // custom hook created for the navbar scroll functionality 
    const { scrolled } = useGlobalContext();
    
    
    
    // fetching the data from the redux store 
    const data = useSelector((state) => state.product.item)
    console.log(data.prices)
    
    // initial value for the price 
    const [amount, setAmount] = useState(0);
    const [extras, setExtras] = useState([]);
    const [quantity, setQuantity] = useState(1)

    const [price, setPrice] = useState(data?.prices !== undefined ? data?.prices[0] : null)//for testing purpose
    
    const changePrice = (number) => {
        setPrice(price + number)
    }

    const handlePrice = (priceIndex) => {
        const difference = data.prices[priceIndex] - data.prices[amount];
        //initially the amount is 0 so if the priceIndex is 0 so the difference will be 0 and we will get the setAmount(0). if the price index is 1 the 1 - 0 the difference is 1 and the setAmount will be the element on position 1.
        setAmount(priceIndex);
        changePrice(difference)
    }


    // checkbox function for the extra items 
    const handleChange = (e, item) => {
        const checked = e.target.checked;
        if (checked) {
            changePrice(item.price);
            setExtras((prev) => [...prev, item])
        } else {
            changePrice(-item.price);
            setExtras(extras.filter(extra => extra._id !== item._id));
        }
    }

    return (
        <>
            {
                data?.length <= 0 ? (
                    <Loading />
                ) : (

                    <ProductContainer>
                        <BgContainer
                            sx={{ background: `url(${data.img})` }}
                        ></BgContainer>
                        {/* navbar start  */}
                        <Navbar scrolled={scrolled} />
                        {/* navbar end */}

                        {/* product page main container start  */}
                        <Container container>
                            <Left item xs={12} lg={6} md={6} sm={12} >
                                {/* product image start  */}
                                <ImageContainer>
                                    <img src={data.img} alt="product image" />
                                </ImageContainer>
                                {/* product image end  */}
                            </Left>
                            <Right item lg={6} md={6} sm={12} xs={12} >
                                {/* product title start  */}
                                <TitleContainer>
                                    <h1>{data.title}</h1>
                                    <Box sx={{ color: data.category === "Nonveg" ? "red" : "green" }} >
                                        <span>{data.category}</span>
                                        <CircleIcon />
                                    </Box>
                                </TitleContainer>
                                {/* product title end  */}

                                {/* product price start  */}
                                {
                                    data?.prices && (
                                        <Typography>Rs.{price}</Typography>
                                    )
                                }
                                {/* product price end */}

                                <h6>Choose meal</h6>

                                {/* buttons to select the price according to the meal portion start  */}
                                <Box>
                                    <Button size="large" onClick={() => handlePrice(0)} variant='contained'>Half</Button>&nbsp;
                                    <Button size="large" onClick={() => handlePrice(1)} variant='contained'>Full</Button>
                                </Box>
                                {/* buttons to select the price according to the meal portion end */}

                                {/* product description start  */}
                                <Box>
                                    <em>&quot;{data.desc}&quot;</em>
                                </Box>
                                {/* product description end  */}

                                {/* extra items container start  */}
                                <ExtraContainer>
                                    <Box>Extra</Box>
                                    {
                                        data?.extra?.map((item) => (
                                            <Box key={item._id} >
                                                <input type="checkbox"
                                                    name={item.text}
                                                    id={item.text}
                                                    onChange={(e) => handleChange(e, item)}
                                                />
                                                <label htmlFor={item.text}>{item.text}</label>&nbsp;&nbsp;
                                                <span>Rs. {item.price}</span>
                                            </Box>
                                        ))
                                    }
                                </ExtraContainer>
                                {/* extra items container end  */}

                                {/* add to cart section start  */}
                                <AddToCart>
                                    <input type="number" defaultValue={1} onChange={(e) => setQuantity(e.target.value)} />
                                    <Button variant="contained" size="medium" >Add to Cart</Button>
                                </AddToCart>
                                {/* add to cart section end  */}
                            </Right>
                        </Container>
                        {/* product page main container end  */}
                    </ProductContainer>
                )
            }
        </>
    )
}

export default Product
