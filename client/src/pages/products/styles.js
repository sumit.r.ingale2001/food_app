/* eslint-disable no-unused-vars */
// styles for the product page

import { Box, FormGroup, Grid, styled } from '@mui/material'

export const BgContainer = styled(Box)(({ theme }) => ({
    filter: "grayscale(0.5)",
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    backgroundRepeat: "no-repeat !important",
    backgroundPosition: "center !important ",
    backgroundSize: "cover !important"
}))

export const ProductContainer = styled(Box)(({ theme }) => ({
    position: "relative",
    height: "100vh",
    [theme.breakpoints.down("md")]: {
        height: "auto",
    }
}))

export const Container = styled(Grid)(({ theme }) => ({
    background: "rgba(0,0,0,0.8)",
    backdropFilter: "blur(10px)",
    height: "100%",
}))

export const Left = styled(Grid)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "1rem",
    background: "rgba(225,225,225,0.1)",
    [theme.breakpoints.down("md")]: {
        paddingTop: "8rem"
    }
}))

export const Right = styled(Grid)(({ theme }) => ({
    background: "rgba(225,225,225,0.1)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "start",
    padding: "1.2rem",
    color: "white",
    "&>*": {
        marginTop: "0.8rem"
    },
    "h1": {
        fontSize: "3rem",
        textAlign: "center"
    },
    "em": {
        fontSize: "1.5rem",
        [theme.breakpoints.down("md")]: {
            fontSize: "1rem"
        }
    },
    "& > div": {
        display: "flex",
        alignItems: "center",
        gap: 3
    },
    "h6": {
        fontSize: "1.3rem",
    },
    "&  button": {
        background: "#EF5350 !important"
    },
    "p": {
        fontSize: "2rem",
        fontWeight: "bold",
        color: "lightgreen"
    },
    "& input[checkbox]": {
        background: "white",
    },
    [theme.breakpoints.down("sm")]: {
        "p": {
            fontSize: "1rem"
        }
    }
}))

export const ImageContainer = styled(Box)(({ theme }) => ({
    width: "500px",
    height: "500px",
    borderRadius: "50%",
    overflow: "hidden",
    "& > img": {
        width: "100%",
        height: "100%",
        objectFit: "cover",
        objectPosition: "center"
    },
    [theme.breakpoints.down("md")]: {
        height: "300px",
        width: "300px"
    },
    [theme.breakpoints.down("sm")]: {
        height: "200px",
        width: "200px"
    }
}))

export const ExtraContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    flexDirection: "column",
    fontSize: "1.3rem",
    "& input": {
        height: "20px",
        width: "20px",
        cursor: "pointer",
        marginRight: "10px"
    },
    "& > div": {
        display: "flex",
        justifyContent: "start",
        alignItems: "center",
        width: "100%",
        "& > span": {
            color: "lightgreen",
            fontWeight: "bold"
        }
    },
}))

export const TitleContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    width: "100%",
    "&>h1": {
        marginRight: "15px",
    },
    "& > div": {
        display: "flex",
        alignItems: "center",
        "& > span": {
            marginRight: "5px"
        }
    },
    [theme.breakpoints.down("md")]: {
        "& h1": {
            fontSize: "1.8rem !important"
        }
    },
    [theme.breakpoints.down("sm")]: {
        justifyContent: "space-between"
    }
}));

export const AddToCart = styled(Box)(({ theme }) => ({
    height: "40px",
    "&  input": {
        marginRight: "8px",
        height: "100%",
        background: "rgba(225,225,225,0.8) !important",
        border: "none",
        outline: "none",
        width: "200px",
        padding: "0 1rem",
        fontSize: "1.1rem"
    },
    "& > button": {
        height: "100%",
    },
    [theme.breakpoints.down("sm")]: {
        "& input": {
            width: "100px"
        },
        "& button": {
            width: "120px",
            fontSize: "10px"
        }
    }
}))
