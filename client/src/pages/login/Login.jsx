import { Container } from './styles'
import { Form, Bg } from '../../components/index'

const Login = () => {

    return (
        // login container start 
        <Container>
            {/* background start  */}
            <Bg />
            {/* background end  */}

            {/* login form start  */}
            <Form
                heading='Login'
                password
                email
                btn
                phoneNumber
                btnText='Login'
                linkText='Signup'
                link='signup'
                text='Not a member?'
            />
            {/* login form end  */}
        </Container>
        // login container end 
    )
}

export default Login
