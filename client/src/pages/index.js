// common export file for the pages folder 

export { default as Signup } from './signup/Signup';
export { default as Login } from './login/Login';
export { default as Home } from './homepage/Home'
export { default as Product } from './products/Product'
export { default as Cart } from './cart/Cart'
export { default as Orders } from './orders/Orders'
