export const data = [
    {
        name: "Butter Chicken",
        prices: "300",
        category: "Nonveg",
        img: "https://images.unsplash.com/photo-1603894584373-5ac82b2ae398?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        desc: "Warm your soul with every bite of butter chicken."
    },
    {
        name: "Paneer Tikka",
        prices: "300",
        category: "Veg",
        img: "https://images.unsplash.com/photo-1551881192-002e02ad3d87?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        desc: "Shahi paneer tikka"
    },
    {
        name: "Pizza",
        prices: "350",
        category: "Nonveg",
        img: "https://images.unsplash.com/photo-1604382354936-07c5d9983bd3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        desc: "Pepper barbecue Chicken I cheese."
    },
];


export const carouselImages = [
    { img: "https://images.unsplash.com/photo-1588594907321-8abb3e506564?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=872&q=80" },

    { img: "https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg" },

    { img: "https://images.pexels.com/photos/958545/pexels-photo-958545.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" },

    { img: "https://images.pexels.com/photos/2228559/pexels-photo-2228559.jpeg" },

    { img: "https://images.pexels.com/photos/3756498/pexels-photo-3756498.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" },

    { img: "https://images.pexels.com/photos/821403/pexels-photo-821403.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" },
];


export const URL = "http://localhost:8000"
