/* eslint-disable react/prop-types */
import React, { useContext, useState } from "react";

// creating a context api 
export const AppContext = React.createContext()

export const AppProvider = ({ children }) => {
    const [scrolled, setScrolled] = useState(false);

    // function for the navbar 
    window.onscroll = () => {
        if (window.pageYOffset === 0 ? setScrolled(false) : setScrolled(true));
        return () => window.onscroll = null
    }
    return <AppContext.Provider value={{ scrolled }} >{children}</AppContext.Provider>
}

// custom hook 
export const useGlobalContext = () => {
    return useContext(AppContext);
}