import { configureStore, createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { URL } from '../data';
import axios from 'axios'


// creating a initial state for the product or food items 
const initialState = {
    items: [],
    item: {},
};



// function to get all the products from backend start 
export const getAllProducts = createAsyncThunk("products/all", async () => {
    try {
        const { data } = await axios.get(`${URL}/product`);
        return data;
    } catch (error) {
        console.log(error, "error while calling getAllProducts api")
    }
})
// function to get all the products from backend end


// function to get the data or food item by id start 
export const getDataById = createAsyncThunk("products/singleItem", async (id) => {
    try {
        const { data } = await axios.get(`${URL}/product/${id}`)
        return data;
    } catch (error) {
        console.log(error, "error while calling getDataById api")
    }
})
// function to get the data or food item by id end


// creating a slice 
const ProductSlice = createSlice({
    name: "product",
    initialState,
    extraReducers: (builders) => {
        builders.addCase(getDataById.fulfilled, (state, action) => {
            state.item = action.payload
        });
        builders.addCase(getAllProducts.fulfilled, (state, action) => {
            state.items = action.payload
        });
    }
})


// creating a store 
export const store = configureStore({
    reducer: {
        product: ProductSlice.reducer,
    }
})